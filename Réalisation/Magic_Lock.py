#!/usr/bin/python3
# -*- coding: utf-8 -*

##IMPORT - START##
from sense_hat import SenseHat
from os import system
import crypto
import time
##IMPORT - END##
#
##VARIABLE ESTABLISHMENT 01 - START##
sense = SenseHat()
sense.low_light = True
current_menu = []
##VARIABLE ESTABLISHMENT 01 - END##
#
##COLOR - START##
R = (255, 0, 0) #RED
G = (0, 255, 0) #GREEN
B = (0, 0, 255) #BLUE
W = (255, 255, 255) #WHITE
Y = (255, 255, 0) #YELLOW
H = (128,128,128) #GREY
N = (128, 0, 0) #BROWN
F = (255, 69, 0) #ORANGE
P = (255,105, 180) #PINK
O = (0, 0, 0) #BLACK
##COLOR - END##
#
##LOGO - START##
logo_black = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  
logo_isok_complete = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, G,
    O, O, O, O, O, O, G, G,
    O, G, O, O, O, G, G, G,
    G, G, G, O, G, G, G, O,
    O, G, G, G, G, G, O, O,
    O, O, G, G, G, O, O, O,
    O, O, O, G, O, O, O, O,
    ]

#---------------------------#
def start_logo_01():
  """
  Display a red light animation when the Raspberry is ON.
  
  @pre: -
  @post: Display a red light animation.
  """
  logo_red_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, R, R, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]

  logo_orange_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, F, F, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]

  logo_green_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, G, G, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]
  sense.set_pixels(logo_red_light)
  time.sleep(.4)
  sense.set_pixels(logo_orange_light)
  time.sleep(.5)
  sense.set_pixels(logo_green_light)
  time.sleep(.6)  
  
def start_logo_02():
  """
  Display a start animation when the Raspberry is ON.
  
  @pre: -
  @post: Display a start animation.
  """
  logo_start_01 = [
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    ]
    
  logo_start_02 = [
    W, W, W, H, H, W, W, W,
    W, W, W, O, O, W, W, W,
    W, W, W, O, O, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    ]
    
  logo_start_03 = [
    W, W, O, H, H, O, W, W,
    W, W, H, O, O, H, W, W,
    W, W, H, O, O, H, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    ]
    
  logo_start_04 = [
    W, O, O, H, H, O, O, W,
    W, O, H, O, O, H, O, W,
    W, O, H, O, O, H, O, W,
    W, O, Y, Y, Y, Y, O, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, O, Y, Y, Y, Y, O, W,
    ]
    
  sense.set_pixels(logo_start_01)
  time.sleep(.15)
  sense.set_pixels(logo_start_02)
  time.sleep(.1)
  sense.set_pixels(logo_start_03)
  time.sleep(.1)
  sense.set_pixels(logo_start_04)
  time.sleep(.1)
  
def end_logo():
  """
  Display an ending animation when the Raspberry is shutdown.
  
  @pre: -
  @post: Display an ending animation.
  """
  logo_end_01 = [
    O, O, O, O, O, O, O, O,
    O, R, O, R, R, O, R, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_02 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_03 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_04 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_05 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_06 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_07 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, R, R, R, R, O, O
    ]
    
  sense.set_pixels(logo_end_01)
  time.sleep(.15)
  sense.set_pixels(logo_end_02)
  time.sleep(.1)
  sense.set_pixels(logo_end_03)
  time.sleep(.1)
  sense.set_pixels(logo_end_04)
  time.sleep(.1)
  sense.set_pixels(logo_end_05)
  time.sleep(.1)
  sense.set_pixels(logo_end_06)
  time.sleep(.1)
  sense.set_pixels(logo_end_07)
  time.sleep(.1)
  sense.set_pixels(logo_black)
  time.sleep(.2)
  
def logo_isok():
  """
  Display a green check.
  
  @pre: -
  @post: Display a green check.
  """
  logo_isok_01 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  logo_isok_02 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  logo_isok_03 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, O, O, O, O,
    O, G, G, G, O, O, O, O,
    O, O, G, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  logo_isok_03 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, O, O, O, O,
    O, G, G, G, O, O, O, O,
    O, O, G, G, G, O, O, O,
    O, O, O, G, O, O, O, O,
    ]
  logo_isok_04 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, G, G, O, O,
    O, G, G, G, G, G, O, O,
    O, O, G, G, G, O, O, O,
    O, O, O, G, O, O, O, O,
    ]
    
  sense.set_pixels(logo_isok_01)
  time.sleep(.01)
  sense.set_pixels(logo_isok_02)
  time.sleep(.03)
  sense.set_pixels(logo_isok_03)
  time.sleep(.08)
  sense.set_pixels(logo_isok_04)
  time.sleep(.1)
  sense.set_pixels(logo_isok_complete)
  time.sleep(.5)
  
#-------------------------#
logo_lock = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, H, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_unlock_lock = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, O, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_unlock = [
    O, O, O, H, H, O, O, O,
    O, O, O, O, O, H, O, O,
    O, O, O, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_show_msg = [
    W, W, W, W, W, W, W, W,
    W, O, O, O, O, O, O, W,
    W, W, O, O, O, O, W, W,
    W, O, W, O, O, W, O, W,
    W, O, O, W, W, O, O, W,
    W, O, O, O, O, O, O, W,
    W, O, O, O, O, O, O, W,
    W, W, W, W, W, W, W, W
    ]

logo_change_msg = [
    W, W, W, W, W, W, W, W,
    W, O, O, O, O, O, O, W,
    W, Y, O, O, O, O, Y, W,
    W, O, Y, O, O, Y, O, W,
    W, O, O, Y, Y, O, O, W,
    W, O, O, O, O, O, O, W,
    W, O, O, O, O, O, O, W,
    W, W, W, W, W, W, W, W
    ]

logo_delete_msg = [
    R, W, W, W, W, W, W, R,
    W, R, O, O, O, O, R, W,
    W, W, R, O, O, R, W, W,
    W, O, W, R, R, W, O, W,
    W, O, O, R, R, O, O, W,
    W, O, R, O, O, R, O, W,
    W, R, O, O, O, O, R, W,
    R, W, W, W, W, W, W, R
    ]

logo_sword = [
    O, O, O, O, O, O, H, H,
    O, O, O, O, O, H, H, H,
    O, O, O, O, H, H, H, O,
    N, N, O, H, H, H, O, O,
    O, N, H, H, H, O, O, O,
    O, O, N, H, O, O, O, O,
    O, N, O, N, N, O, O, O,
    N, O, O, O, N, O, O, O,
    ]

logo_key = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, O, H, H, O, O, O,
    O, O, O, H, O, O, O, O,
    O, O, O, H, O, O, O, O,
    O, O, O, H, H, H, O, O,
    O, O, O, H, O, O, O, O,
    O, O, O, H, H, H, O, O,
    ]

logo_edit_letter_msg = [
    O, O, O, O, O, O, O, O,
    O, O, R, O, O, O, O, O,
    O, R, R, R, O, O, O, O,
    O, O, R, R, R, O, O, O,
    O, O, O, R, R, R, O, O,
    O, O, O, O, R, R, R, O,
    O, O, O, O, O, R, P, O,
    O, O, O, O, O, O, O, O,
    ]

logo_edit = [
    O, O, O, O, O, O, O, O,
    O, O, G, O, O, O, O, O,
    O, G, G, G, O, O, O, O,
    O, O, G, G, G, O, O, O,
    O, O, O, G, G, G, O, O,
    O, O, O, O, G, G, G, O,
    O, O, O, O, O, G, P, O,
    O, O, O, O, O, O, O, O,
    ]

logo_delete = [
    R, R, O, O, O, O, R, R,
    R, R, R, O, O, R, R, R,
    O, R, R, R, R, R, R, O,
    O, O, R, R, R, R, O, O,
    O, O, R, R, R, R, O, O,
    O, R, R, R, R, R, R, O,
    R, R, R, O, O, R, R, R,
    R, R, O, O, O, O, R, R,
    ]

logo_back = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, W, O,
    O, O, R, O, O, W, W, O,
    O, R, W, W, W, W, O, O,
    O, O, R, O, O, O, O, O,
    ]

logo_exit = [
    O, O, O, R, R, O, O, O,
    O, R, O, R, R, O, R, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
##LOGO - END##
#
##LOGOS POSITIONS - START##
logo_null = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, W, W, O, O, O,
    O, O, O, W, W, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]

logo_arrow_right = [
    O, O, O, O, W, O, O, O,
    O, O, O, O, O, W, O, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, O, W,
    O, O, O, O, O, O, O, W,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, W, O, O,
    O, O, O, O, W, O, O, O,
    ]

logo_arrow_left = [
    O, O, O, W, O, O, O, O,
    O, O, W, O, O, O, O, O,
    O, W, O, O, O, O, O, O,
    W, O, O, O, O, O, O, O,
    W, O, O, O, O, O, O, O,
    O, W, O, O, O, O, O, O,
    O, O, W, O, O, O, O, O,
    O, O, O, W, O, O, O, O,
    ]

logo_arrow_up = [
    O, O, O, W, W, O, O, O,
    O, O, W, O, O, W, O, O,
    O, W, O, O, O, O, W, O,
    W, O, O, O, O, O, O, W,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]

logo_arrow_down = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    W, O, O, O, O, O, O, W,
    O, W, O, O, O, O, W, O,
    O, O, W, O, O, W, O, O,
    O, O, O, W, W, O, O, O,
    ]
##LOGOS POSITIONS - END##
#
##OTHERS FUNCTIONS - START##
def list_to_str (lst):
    """
    Change a list to a string.
    
    @pre: 'lst' is a list.
    @post: return the previous list to a string.
    """
    carac = ""
    for g in range(len(lst)):
        carac += lst[g]
    return carac

def confirme_action():
    """
    Ask the user to confirm the action.
    
    @pre: -
    @post: return a boolean (False if a cross is display and True if a check is display).
    """
    sense.show_message("?", scroll_speed = 0.1)
    selection = True
    index = 0
    sense.set_pixels(logo_delete)
    
    current_logo = [logo_delete, logo_isok_complete]
    while selection:
      events = sense.stick.get_events()
      
      for event in events:
        if event.action != "released":
          if event.direction == "left":
            index -= 1
            
          elif event.direction == "right":
            index += 1
            
          elif event.direction == "middle":
            selection = False
            
          
          if selection:
            current_mode = current_logo[index % 2]
            sense.set_pixels(current_mode)
            
            
      if not selection:
        current_mode = current_logo[index % 2]
        sense.set_pixels(current_mode)
        
    if (index % len(current_logo)) == 1:
      return True
    else:
      return False
        
##OTHERS FUNCTIONS - END##
#
##TAKE PWD AND MSG IN A FILE - START##
def read_encrypted_pwd():
    """
    Reads the "godfather_secret" file.
    
    @pre: -
    @post: return the hashed password in the "godfather_secret" file.
           If the file doesn't exist, it is create and it returns "".
    """
    try:
        with open ("godfather_secret", "r") as pwd_file:
                pwd = pwd_file.read()
                return pwd
            
    except:
        with open ("godfather_secret", "w") as pwd_file:
            pwd_file.write("")
            return ""


def write_encrypted_pwd(pwd):
    """
    Written in the file "godfather_secret" and hash the password.
    
    @pre: 'pwd' is a string
    @post: hashs the password and writes it in the "godfather_secret" file.
           Print "OK" or "ERROR" depending on the result.
    """
    with open ("godfather_secret", "w") as pwd_file:
        try:
            pwd_encrypted = crypto.hashing(pwd)
            print("OK")
            pwd_file.write(pwd_encrypted)
            
        except:
            print("ERROR")


def read_encrypted_msg():
    """
    Reads the file "godfather_request" and decrypt the password.
    
    @pre: -
    @post: return the decrypted message in the "godfather_request" file.
           If the file doesn't exist, it is create and it returns "".
           If there is any password in the "godfather_secret" file, it returns "" and the file content is deleted.
    """
    try:
        with open ("godfather_request", "r") as msg_file:
                msg = msg_file.read()
                pwd = read_encrypted_pwd()
                msg_decrypted = crypto.decode(pwd, msg)
                return msg_decrypted
            
    except:
        with open ("godfather_request", "w") as msg_file:
            msg_file.write("")
            return ""
           
def write_encrypted_msg(msg):
    """
    Written in the file "godfather_request" and encrypt the message.
    
    @pre: 'msg' is a string
    @post: encrypt the message and writes it in the "godfather_request" file.
           Print "OK" or "ERROR" depending on the result.
    """
    with open ("godfather_request", "w") as msg_file:
        try:
            pwd = read_encrypted_pwd()
            msg = msg.upper()
            msg_encrypted = crypto.encode(pwd, msg)
            print("OK")
            msg_file.write(msg_encrypted)
            
        except:
            print("ERROR")
        
##TAKE PWD AND MSG IN A FILE - END##
#
##SAVE PWD IN A LIST - START##
def save_pwd(p, lst_pwd):
    """
    Save a letter in a password list.
    
    @pre: 'lst_pwd' is a list.
          'p' is a letter which will be added in the list 'lst_pwd'.
    @post: return the list 'lst_pwd' with the letter 'p' added in it.
    """
    if p == "G":
        print(lst_pwd)
        lst_pwd.append("G")
        time.sleep(2)
        return lst_pwd
        
    elif p == "D":
        print(lst_pwd)
        lst_pwd.append("D")
        time.sleep(2)
        return lst_pwd
        
    elif p == "R":
        print(lst_pwd)
        lst_pwd.append("R")
        time.sleep(2)
        return lst_pwd
        
    elif p == "F":
        print(lst_pwd)
        lst_pwd.append("F")
        time.sleep(2)
        return lst_pwd
        
    elif p == "N":
        time.sleep(1)
        print(lst_pwd)
        events = sense.stick.get_events()
        for event in events:
            if event.direction == "middle" and event.action == "pressed":
                print(lst_pwd)
                return [lst_pwd, False]
        return lst_pwd
                
            

def enter_pwd():
    """
    Allows to create a password thanks to the gyroscope.
    Use the gyroscope sensor to compute the Raspberry's orientation.
    Call the save_pwd function to save the orientation.
    
    @pre: -
    @post: return the list 'lst_pwd'.
    """
    lst_pwd = []
    not_exit = True
    compt = 10
    while compt != 0:
        events = sense.stick.get_events()
        compt -= 1
    while not_exit:
        #get the orientation 
        acceleration = sense.get_accelerometer_raw()
        x = acceleration['x']
        y = acceleration['y']
        z = acceleration['z']

        x=round(x, 0)
        y=round(y, 0)
        z=round(z, 0)
        
        #Update the rotation of the display depending on which way up the Sense HAT is
        if x  == -1:
          p = "G"
          sense.set_pixels(logo_arrow_left)
          lst_pwd = save_pwd(p, lst_pwd)

        if x == 1 :
          p = "D"
          sense.set_pixels(logo_arrow_right)
          lst_pwd = save_pwd(p, lst_pwd)

        if y == 1:
          p = "R"
          sense.set_pixels(logo_arrow_down)
          lst_pwd = save_pwd(p, lst_pwd)

        if y == -1:
          p = "F"
          sense.set_pixels(logo_arrow_up)
          lst_pwd = save_pwd(p, lst_pwd)

        if -1<x<1 and -1<y<1:
          p = "N"
          sense.set_pixels(logo_null)
          temp = save_pwd(p, lst_pwd)
          if len(temp) == 2 and temp[1] == False:
            lst_pwd = temp[0]
            not_exit = temp[1]
          else:
            lst_pwd = save_pwd(p, lst_pwd)

    return lst_pwd

##SAVE PWD IN A LIST - END##
#
##[MAIN MENU] - LOCK & UNLOCK MENU -- START##
def unlock_menu(unlock_mode):
  """
  MAIN MENU 02:
  Show an unlock menu when the user can show the message, change the message and the password,
  delete the message, lock the Raspberry (go to the lock menu) and shutdown the Raspberry.
  
  @pre: 'unlock_mode' is a list of strings.
        The user must to unlock the 'lock_menu'.
  @post: Allow the user to choose a mode.
  """
  if unlock_mode == "show_msg":
    sense.set_pixels(logo_show_msg)
    if enter_in_mode:
      msg = read_encrypted_msg()
      sense.show_message(msg)
   
  if unlock_mode == "change_msg":
    sense.set_pixels(logo_change_msg)
    if enter_in_mode:
      msg = read_encrypted_msg() #string + decrypt msg
      print(msg)
      if msg == "" or msg == None:
        msg = []
      else:
        msg = list(msg) #string to list
      msg_lst = alphabet_function(msg)
      print(msg_lst)
      msg_modified = list_to_str(msg_lst) # list to str
      print(msg_modified)
      write_encrypted_msg(msg_modified)
      
  elif unlock_mode == "delete_msg":
    sense.set_pixels(logo_delete_msg)
    if enter_in_mode:
        if confirme_action():
            write_encrypted_msg("")
            logo_isok()
    
  elif unlock_mode == "change_password":
    sense.set_pixels(logo_key)
    if enter_in_mode:
        if confirme_action():
            pwd_lst = enter_pwd()
            pwd = list_to_str(pwd_lst) #list to str
            write_encrypted_pwd(pwd) # hash password
            logo_isok()

  elif unlock_mode == "lock":
    sense.set_pixels(logo_unlock)
    if enter_in_mode:
      current_menu[0:1] = [lock_menu]
      current_menu[1:2] = [lock_mode]
      sense.set_pixels(logo_unlock_lock)
      time.sleep(.2)
      sense.set_pixels(logo_lock)
      time.sleep(.2)
    
  elif unlock_mode == "exit":
    sense.set_pixels(logo_exit)
    if enter_in_mode:
      if confirme_action():
        end_logo()
        system("sudo shutdown now")
   
#-----------------------------------------#
def lock_menu(lock_mode):
  """
  MAIN MENU 01:
  Show a lock menu when the user can unlock the Raspberry (go to the unlock mode) and shutdown the Raspberry.
  
  @pre: 'lock_mode' is a list of strings.
  @post: Allow the user to choose a mode.
  """
  if lock_mode == "unlock":
    sense.set_pixels(logo_lock)
    
    if enter_in_mode:
      #Saved Password
      pwd_saved = read_encrypted_pwd() #string hashed
      print(pwd_saved)
      
      #Current Password
      lst_pwd = enter_pwd()
      pwd = list_to_str(lst_pwd) #list to string
      pwd = crypto.hashing(pwd) #encrypt current password to compare with saved password 
      print(pwd)
      
      if pwd == pwd_saved or pwd_saved == "":
        logo_isok()
        sense.set_pixels(logo_lock)
        time.sleep(.2)
        current_menu[0:1] = [unlock_menu]
        current_menu[1:2] = [unlock_mode]
        sense.set_pixels(logo_unlock_lock)
        time.sleep(.2)
        sense.set_pixels(logo_unlock)
        time.sleep(.2)
      else:
        sense.set_pixels(logo_delete)
        time.sleep(.5)
    
  elif lock_mode == "exit":
    sense.set_pixels(logo_exit)
    if enter_in_mode:
      if confirme_action():
        end_logo()
        system("sudo shutdown now")
##[MAIN MENU] - LOCK & UNLOCK MENU -- END##
#
##ALPHABET - START##
alphabet_list_and_mods =  [
    ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    ["0","1","2","3","4","5","6","7","8","9"],
    ["key",logo_show_msg],
    ["edit",logo_edit_letter_msg],
    ["delete",logo_delete],
    ["back",logo_back]
    ]
    

alphabet_list = [
    ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    ["0","1","2","3","4","5","6","7","8","9","0"]
    ]


def alphabet_function(msg):
  """
  Allow the user to enter a message with letters and numbers.
  There is four options: show the message, edit a letter, delete a letter and save & back to the 'unlock_menu'.
  
  @pre: 'msg' is a list of strings (which one string is a letter or a number).
  @post: return the list 'msg' modified.
  """
  n = 0
  p = 0
  
  sense.show_letter("A")
  
  while True:
    event = sense.stick.wait_for_event()
    if event.direction == "down" and event.action == "released":
      #Change list 
      n += 1 
          
    if event.direction == "up" and event.action == "pressed": 
      #Change list
      n -= 1
            
    if event.direction =="right" and (event.action == "pressed" or event.action == "held"): 
      #Change list index
      p += 1
          
    if event.direction == "left" and (event.action == "pressed" or event.action == "held"): 
      #Change list index
      p -=1

    n = n%len(alphabet_list_and_mods)
    
    if n < 3: 
      #letters and numbers
      index = p%len(alphabet_list_and_mods[n])
      sense.show_letter(alphabet_list_and_mods[n][index])
      
      if event.action == "pressed" and event.direction == "middle":
        msg.append(alphabet_list_and_mods[n][index])
        
    else:
      #mods
      sense.set_pixels(alphabet_list_and_mods[n][1])
      if event.action == "pressed" and event.direction == "middle":
          
        if alphabet_list_and_mods[n][0] == "key":
          sense.show_message(msg)
          print(msg)
          
        elif alphabet_list_and_mods[n][0] == "edit":
          edit_code(msg)
          
        elif alphabet_list_and_mods[n][0] == "delete":
          del_code(msg)
          
        elif alphabet_list_and_mods[n][0] == "back":
          return msg
          
     
def edit_code(msg):
    """
    This is a menu to edit (change or delete) a message's letter.
    
    @pre: 'msg' is a list of strings (which one string is a letter or a number).
    @post: return the 'msg' list modified.
    """
    n = 0 
    index = 0
    p = 0
    
    try:
     sense.show_letter(msg[index])
    except:
      sense.show_message("Edit a code")
      
    while True:
        
      if not msg:
        break
      else:
        event = sense.stick.wait_for_event()
        if event.direction == "right" and event.action == "pressed":
          if index < len(msg)-1:
            index += 1 
          sense.show_letter(msg[index])
          
        if event.direction == "left" and event.action == "pressed":
          if index > 0:
            index -= 1
          try :
            sense.show_letter(msg[index])
          except :
            break
        
        if event.action =="pressed" and event.direction == "middle":
          modify = modifier_code(msg, index)
          modify
          break
          
          
def modifier_code(msg, index):
  """
  Allow the user to change a letter or number of his own message.
  
  @pre: 'msg' is a list of strings (which one string is a letter or a number).
        'index' is an integer which refers to the index's letter in the user's message.
  @post: return the 'msg' list modified.
  """
  n = 0 
  c = 0
  p = 0
  while 1 > c:
    sense.show_letter(alphabet_list[n][p])
    event = sense.stick.wait_for_event()
    
    if event.action =="pressed" and event.direction == "right":
      if p < len(alphabet_list[n])-1:
        p+= 1 
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "left":
      if p > 0:
        p-= 1 
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "up":
      if n > 0:
        n-=1
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "down":
      if n < len(alphabet_list)-1:
        n+= 1 
      try:
        sense.show_letter(alphabet_list[n][p])
      except:
        p = 0 
        sense.show_letter(alphabet_list[n][p])
        
    if event.action == "pressed" and event.direction == "middle":
      msg[index] = alphabet_list[n][p]
      logo_isok()
      return msg
         
         
def del_code(msg):
  """
  Delete a letter in the user's message.
  
  @pre: 'msg' is a list of strings (which one string is a letter or a number).
  @post: return the 'msg' list modified.
  """
  n = 0 
  c = 0
  p = 0
  
  while 1 > c:
    try:
      sense.show_letter(msg[p])
      
    except:
      try:
        p = 0 
        sense.show_letter(msg[p])
        
      except: 
        sense.show_message("Edit a code")
        break
    
    event = sense.stick.wait_for_event()
    
    if event.action =="pressed" and event.direction == "right":
      if p < len(msg)-1:
        p+= 1 
      sense.show_letter(msg[p])
      
    if event.action =="pressed" and event.direction == "left":
      if p > 0:
        p-= 1 
      sense.show_letter(msg[p])
      
    if event.action == "pressed" and event.direction == "middle":
      del msg[p]
      logo_isok()
      return msg

##ALPHABET - END##
#
##VARIABLE ESTABLISHMENT 02 - START##
index = 0
unlock_mode = ["show_msg", "change_msg", "delete_msg", "change_password", "lock", "exit"]
lock_mode = ["unlock", "exit"]
current_menu.append(lock_menu)
current_menu.append(lock_mode)
##VARIABLE ESTABLISHMENT 02 - END##
#
##PROGRAM START - START##
start_logo_01()
start_logo_02()
while True:
  selection = False
  enter_in_mode = False
  events = sense.stick.get_events()
  
  for event in events:
    if event.action != "released":
      if event.direction == "left":
        index -= 1
        selection = True
        
      elif event.direction == "right":
        index += 1
        selection = True
        
      elif event.direction == "middle":
        enter_in_mode = True
        
      
      if selection:
        current_mode = current_menu[1][index % len(current_menu[1])]
        if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
          index = 0
        current_menu[0](current_mode)
        
        
  if not selection:
    current_mode = current_menu[1][index % len(current_menu[1])]
    if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
        index = 0
    current_menu[0](current_mode)
##PROGRAM START - END##
