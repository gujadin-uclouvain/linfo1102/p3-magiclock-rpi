##IMPORT - START##
from sense_hat import SenseHat
from os import system
import time
##IMPORT - END##

##VARIABLE ESTABLISHMENT 01 - START##
sense = SenseHat()
sense.low_light = True

current_menu = []
##VARIABLE ESTABLISHMENT 01 - END##

##COLOR - START##
R = (255, 0, 0) #RED
G = (0, 255, 0) #GREEN
B = (0, 0, 255) #BLUE
W = (255, 255, 255) #WHITE
Y = (255, 255, 0) #YELLOW
H = (128,128,128) #GREY
N = (128, 0, 0) #BROWN
O = (0, 0, 0) #BLACK
##COLOR - END##

##LOGO - START##
logo_start = []

logo_lock = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, H, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_unlock_lock = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, O, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_unlock = [
    O, O, O, H, H, O, O, O,
    O, O, O, O, O, H, O, O,
    O, O, O, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_show_msg = [
    W, W, W, W, W, W, W, W,
    W, O, O, O, O, O, O, W,
    W, W, O, O, O, O, W, W,
    W, O, W, O, O, W, O, W,
    W, O, O, W, W, O, O, W,
    W, O, O, O, O, O, O, W,
    W, O, O, O, O, O, O, W,
    W, W, W, W, W, W, W, W
    ]

logo_change_msg = [
    R, W, W, W, W, W, W, R,
    W, R, O, O, O, O, R, W,
    W, W, R, O, O, R, W, W,
    W, O, W, R, R, W, O, W,
    W, O, O, R, R, O, O, W,
    W, O, R, O, O, R, O, W,
    W, R, O, O, O, O, R, W,
    R, W, W, W, W, W, W, R
    ]

logo_change_password = [
    O, O, O, O, O, O, H, H,
    O, O, O, O, O, H, H, H,
    O, O, O, O, H, H, H, O,
    N, N, O, H, H, H, O, O,
    O, N, H, H, H, O, O, O,
    O, O, N, H, O, O, O, O,
    O, N, O, N, N, O, O, O,
    N, O, O, O, N, O, O, O,
    ]

logo_exit = [
    O, O, O, R, R, O, O, O,
    O, R, O, R, R, O, R, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
##LOGO - END##

##[MAIN MENU] - LOCK & UNLOCK MENU -- START##
def unlock_menu(unlock_mode):
  if unlock_mode == "show_msg":
    sense.set_pixels(logo_show_msg)
    if enter_in_mode:
      sense.show_message("Hello")
    
  elif unlock_mode == "change_msg":
    sense.set_pixels(logo_change_msg)
    
  elif unlock_mode == "change_password":
    sense.set_pixels(logo_change_password)
    
  elif unlock_mode == "lock":
    sense.set_pixels(logo_unlock)
    if enter_in_mode:
      current_menu[0:1] = [lock_menu]
      current_menu[1:2] = [lock_mode]
      sense.set_pixels(logo_unlock_lock)
      time.sleep(.2)
      sense.set_pixels(logo_lock)
      time.sleep(.2)
    
  elif unlock_mode == "exit":
    sense.set_pixels(logo_exit)
    if enter_in_mode:
      system("sudo shutdown now")
   
#-----------------------------------------#
def lock_menu(lock_mode):
  if lock_mode == "unlock":
    sense.set_pixels(logo_lock)
    if enter_in_mode:
      current_menu[0:1] = [unlock_menu]
      current_menu[1:2] = [unlock_mode]
      sense.set_pixels(logo_unlock_lock)
      time.sleep(.2)
      sense.set_pixels(logo_unlock)
      time.sleep(.2)
    
  elif lock_mode == "exit":
    sense.set_pixels(logo_exit)
    if enter_in_mode:
      system("sudo shutdown now")
##[MAIN MENU] - LOCK & UNLOCK MENU -- END##

##VARIABLE ESTABLISHMENT 02 - START##
index = 0
unlock_mode = ["show_msg", "change_msg", "change_password", "lock", "exit"]
lock_mode = ["unlock", "exit"]
current_menu.append(lock_menu)
current_menu.append(lock_mode)
##VARIABLE ESTABLISHMENT 02 - END##

##DEMARRAGE DU PROGRAMME##
while True:
  selection = False
  enter_in_mode = False
  events = sense.stick.get_events()
  
  for event in events:
    if event.action != "released":
      if event.direction == "left":
        index -= 1
        selection = True
        
      elif event.direction == "right":
        index += 1
        selection = True
        
      elif event.direction == "middle":
        enter_in_mode = True
        
      
      if selection:
        current_mode = current_menu[1][index % len(current_menu[1])]
        if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
          index = 0
        current_menu[0](current_mode)
        
        
  if not selection:
    current_mode = current_menu[1][index % len(current_menu[1])]
    if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
        index = 0
    current_menu[0](current_mode)
       
