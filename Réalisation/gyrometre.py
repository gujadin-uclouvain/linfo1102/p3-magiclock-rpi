from sense_hat import SenseHat
import time

sense = SenseHat()

# save the code in a list
lst_pwd = []
def save_pwd():
  global lst_pwd
  if p == "G":
    lst_pwd.append("G")
    time.sleep(2)
  elif p == "D":
    lst_pwd.append("D")
    time.sleep(2)
  elif p == "R":
    lst_pwd.append("R")
    time.sleep(2)
  elif p == "F":
    lst_pwd.append("F")
    time.sleep(2)
  elif p == "N":
    event = sense.stick.wait_for_event()
    if event.direction == "middle" and event.action =="pressed":
      sense.show_message(lst_pwd)
  
#main 
while True:
    #get the orientation 
    acceleration = sense.get_accelerometer_raw()
    x = acceleration['x']
    y = acceleration['y']
    z = acceleration['z']

    x=round(x, 0)
    y=round(y, 0)
    z=round(z, 0)
    
    #Update the rotation of the display depending on which way up the Sense HAT is
    if x  == -1:
      p = "G"
      sense.show_letter(p)
      save_pwd()
    if x == 1 :
      p = "D"
      sense.show_letter(p)
      save_pwd()
    if y == 1:
      p = "R"
      sense.show_letter(p)
      save_pwd()
    if y == -1:
      p = "F"
      sense.show_letter(p)
      save_pwd()
    if -1<x<1 and -1<y<1:
      p = "N"
      sense.show_letter(p)
      save_pwd()
