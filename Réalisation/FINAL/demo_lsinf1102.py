#!/usr/bin/python3
# -*- coding: utf-8 -*

##IMPORT - START##
from sense_hat import SenseHat
from os import system
from threading import Timer
import crypto
import time
import logo
##IMPORT - END##
#
##VARIABLE ESTABLISHMENT 01 - START##
sense = SenseHat()
sense.low_light = True
current_menu = []
##VARIABLE ESTABLISHMENT 01 - END##
#
##[MAIN MENU] - LOCK & UNLOCK MENU -- START##
def unlock_menu(unlock_mode):
  """
  MAIN MENU 02:
  Show an unlock menu when the user can show the message, change the message and the password,
  delete the message, lock the Raspberry (go to the lock menu) and shutdown the Raspberry.
  
  @pre: 'unlock_mode' is a list of strings.
        The user must to unlock the 'lock_menu'.
  @post: Allow the user to choose a mode.
  """
  if unlock_mode == "show_msg":
    logo.logo_show_msg()
    if enter_in_mode:
      msg = read_encrypted_msg()
      sense.show_message(msg)
      print("(MAIN MENU) - Message seen...\n")
   
   
  if unlock_mode == "change_msg":
    logo.logo_change_msg()
    
    if enter_in_mode:
      msg = read_encrypted_msg() #string + decrypt msg
      
      if msg is None or msg == "":
        msg = []
      else:
        msg = list(msg) #string to list
        
      msg_lst = alphabet_function(msg)
      msg_modified = list_to_str(msg_lst) # list to str
      write_encrypted_msg(msg_modified)
      print("(MAIN MENU) - Message changed...\n")
      
      
      
  elif unlock_mode == "delete_msg":
    logo.logo_delete_msg()
    if enter_in_mode:
        if confirme_action():
            write_encrypted_msg("", delete_msg = True)
            logo.logo_isok()
            print("(MAIN MENU) - Message deleted...\n")
    
    
  elif unlock_mode == "change_password":
    logo.logo_key()
    if enter_in_mode:
        
        if confirme_action():
            pwd_lst = enter_pwd()
                
            pwd = list_to_str(pwd_lst) #list to str
            write_encrypted_pwd(pwd) # hash password
            logo.logo_isok()
            print("(MAIN MENU) - Password changed...\n")

  elif unlock_mode == "delete_password":
    logo.logo_delete_key()
    if enter_in_mode:
        
        if confirme_action():
            write_encrypted_pwd("")
            logo.logo_isok()

  elif unlock_mode == "lock":
    logo.logo_unlock()
    if enter_in_mode:
      current_menu[0:1] = [lock_menu]
      current_menu[1:2] = [lock_mode]
      logo.logo_unlock_lock()
      time.sleep(.2)
      logo.logo_lock()
      print("(MAIN MENU) - Raspberry Pi locked...\n")
      time.sleep(.2)
    
    
  elif unlock_mode == "exit":
    logo.logo_exit()
    if enter_in_mode:
      if confirme_action():
        print("(MAIN MENU) - Shutdown...\n")
        logo.end_logo()
        while True:
            logo.logo_black()
            system("sudo shutdown now")
            logo.logo_black()
#-----------------------------------------#
def lock_menu(lock_mode):
  """
  MAIN MENU 01:
  Show a lock menu when the user can unlock the Raspberry (go to the unlock mode) and shutdown the Raspberry.
  
  @pre: 'lock_mode' is a list of strings.
  @post: Allow the user to choose a mode.
  """
  if lock_mode == "unlock":
    logo.logo_lock()
    
    if enter_in_mode:
      #Saved Password
      pwd_saved = read_encrypted_pwd() #string hashed
      
      #Current Password
      pwd = ""
      if pwd_saved != "":
          lst_pwd = enter_pwd()
          pwd = list_to_str(lst_pwd) #list to string
          pwd = crypto.hashing(pwd) #encrypt current password to compare with saved password
      
      if pwd == pwd_saved:
        if pwd_saved != "":
            logo.logo_isok()
            logo.logo_lock()
            time.sleep(.2)
        else:
            print("(MAIN MENU) - No password saved...\n")
            
        current_menu[0:1] = [unlock_menu]
        current_menu[1:2] = [unlock_mode]
        logo.logo_unlock_lock()
        time.sleep(.2)
        logo.logo_unlock()
        print("(MAIN MENU) - Raspberry Pi unlocked...\n")
        time.sleep(.2)
      else:
        logo.logo_delete()
        print("(MAIN MENU) - Wrong password...\n")
        time.sleep(.5)
    
    
  elif lock_mode == "exit":
    logo.logo_exit()
    if enter_in_mode:
      if confirme_action():
        print("(MAIN MENU) - Shutdown...\n")
        logo.end_logo()
        while True:
            logo.logo_black()
            system("sudo shutdown now")
            logo.logo_black()
##[MAIN MENU] - LOCK & UNLOCK MENU -- END##
#
##WRITE AND READ FILES - START##
def read_encrypted_pwd():
    """
    Reads the "godfather_secret" file.
    
    @pre: -
    @post: return the hashed password in the "godfather_secret" file.
           If the file doesn't exist, it is create and it returns "".
    """
    try:
        with open ("godfather_secret", "r") as pwd_file:
            pwd = pwd_file.read()
            print("(R&W) - Password read on the file...\n")
            return pwd
            
    except:
        with open ("godfather_secret", "w") as pwd_file:
            pwd_file.write("")
            print("(R&W) - Password - File created...\n")
            return ""


def write_encrypted_pwd(pwd):
    """
    Written in the file "godfather_secret" and hash the password.
    
    @pre: 'pwd' is a string
    @post: hashs the password and writes it in the "godfather_secret" file.
           Print "OK" or "ERROR" depending on the result.
    """
    with open ("godfather_secret", "w") as pwd_file:
        try:
            pwd_encrypted = crypto.hashing(pwd)
            pwd_file.write(pwd_encrypted)
            write_encrypted_msg("", delete_msg = True)
            print("(R&W) - Password written on the file... (W)\n")
            
        except:
            print("(R&W) - Password - File created...\n")


def read_encrypted_msg():
    """
    Reads the file "godfather_request" and decrypt the password.
    
    @pre: -
    @post: return the decrypted message in the "godfather_request" file.
           If the file doesn't exist, it is create and it returns "".
           If there is any password in the "godfather_secret" file, it returns "" and the file content is deleted.
    """
    try:
        with open ("godfather_request", "r") as msg_file:
            msg = msg_file.read()
            print("(R&W) - Message read on the file...")
            pwd = read_encrypted_pwd()
            print("(R&W) - Password read on the file...\n")
            msg_decrypted = crypto.decode(pwd, msg)
            return msg_decrypted
            
    except:
        with open ("godfather_request", "w") as msg_file:
            msg_file.write("")
            print("(R&W) - Message removed from the file\n or\n(R&W) - Message - File created\n")
            return ""
           
def write_encrypted_msg(msg, delete_msg = False):
    """
    Written in the file "godfather_request" and encrypt the message.
    
    @pre: 'msg' is a string
    @post: encrypt the message and writes it in the "godfather_request" file.
           Print "OK" or "ERROR" depending on the result.
    """
    with open ("godfather_request", "w") as msg_file:
        try:
            pwd = read_encrypted_pwd()
            
            if delete_msg == True or (msg is None or msg == ""):
                msg_file.write("")
                print("(R&W) - Message removed from the file...\n")
                return
            
            while pwd is None or pwd == "":
                sense.show_message("Choose a password...", scroll_speed = 0.07)
                print("Choose a password...\n")
                lst_pwd = enter_pwd()
                    
                pwd = list_to_str(lst_pwd) #list to string
                write_encrypted_pwd(pwd)
                pwd = read_encrypted_pwd()
                logo.logo_isok()
                
            msg_encrypted = crypto.encode(pwd, msg)
            msg_file.write(msg_encrypted)
            print("(R&W) - Message written on the file... (W)\n")
            
        except:
            print("(R&W) - ERROR - Message (W)\n")
        
##WRITE AND READ FILES - END##
#
##PASSWORD MANAGER - START##
def save_pwd(p, lst_pwd):
    """
    Save a letter in a password list.
    
    @pre: 'lst_pwd' is a list.
          'p' is a letter which will be added in the list 'lst_pwd'.
    @post: return the list 'lst_pwd' with the letter 'p' added in it.
    """
    if p == "G":
        lst_logo = [logo.logo_arrow_left01, logo.logo_arrow_left02, logo.logo_arrow_left03, logo.logo_arrow_left04, logo.logo_arrow_left05, logo.logo_arrow_left06, logo.logo_arrow_left07, logo.logo_arrow_left08]
        for g in range(len(lst_logo)):
            lst_logo[g]()
            time.sleep(1.5/len(lst_logo))
        time.sleep(.5)
        lst_pwd.append("G")
        return lst_pwd
        
    elif p == "D":
        lst_logo = [logo.logo_arrow_right01, logo.logo_arrow_right02, logo.logo_arrow_right03, logo.logo_arrow_right04, logo.logo_arrow_right05, logo.logo_arrow_right06, logo.logo_arrow_right07, logo.logo_arrow_right08]
        for g in range(len(lst_logo)):
            lst_logo[g]()
            time.sleep(1.5/len(lst_logo))
        time.sleep(.5)
        lst_pwd.append("D")
        return lst_pwd
        
    elif p == "R":
        lst_logo = [logo.logo_arrow_down01, logo.logo_arrow_down02, logo.logo_arrow_down03, logo.logo_arrow_down04, logo.logo_arrow_down05, logo.logo_arrow_down06, logo.logo_arrow_down07, logo.logo_arrow_down08]
        for g in range(len(lst_logo)):
            lst_logo[g]()
            time.sleep(1.5/len(lst_logo))
        time.sleep(.5)
        lst_pwd.append("R")
        return lst_pwd
        
    elif p == "F":
        lst_logo = [logo.logo_arrow_up01, logo.logo_arrow_up02, logo.logo_arrow_up03, logo.logo_arrow_up04, logo.logo_arrow_up05, logo.logo_arrow_up06, logo.logo_arrow_up07, logo.logo_arrow_up08]
        for g in range(len(lst_logo)):
            lst_logo[g]()
            time.sleep(1.5/len(lst_logo))
        time.sleep(.5)
        lst_pwd.append("F")
        return lst_pwd
        
    elif p == "N":
        events = sense.stick.get_events()
        for event in events:
            if event.direction == "up" or event.direction == "down" or event.direction == "left" or event.direction == "right":
                logo_reboot_lst = [logo.logo_null01, logo.logo_null02, logo.logo_null03, logo.logo_null_reboot, logo.logo_null_reboot01, logo.logo_null_reboot02, logo.logo_null_reboot03, logo.logo_null]
                for g in range(len(logo_reboot_lst)):
                    logo_reboot_lst[g]()
                    time.sleep(1/len(logo_reboot_lst))
                print("(PM) - Password reset...\n")
                return []
            elif event.direction == "middle" and event.action == "pressed":
                return [lst_pwd, False]
        return lst_pwd
                     

def enter_pwd():
    """
    Allows to create a password thanks to the gyroscope.
    Use the gyroscope sensor to compute the Raspberry's orientation.
    Call the save_pwd function to save the orientation.
    
    @pre: -
    @post: return the list 'lst_pwd'.
    """
    lst_pwd = []
    not_exit = True
    compt = 10
    while compt != 0:
        events = sense.stick.get_events()
        compt -= 1
    while not_exit:
        #get the orientation 
        acceleration = sense.get_accelerometer_raw()
        x = acceleration['x']
        y = acceleration['y']
        z = acceleration['z']

        x=round(x, 0)
        y=round(y, 0)
        z=round(z, 0)
        
        #Update the rotation of the display depending on which way up the Sense HAT is
        if x  == -1:
          p = "G"
          logo.logo_arrow_left()
          lst_pwd = save_pwd(p, lst_pwd)

        if x == 1 :
          p = "D"
          logo.logo_arrow_right()
          lst_pwd = save_pwd(p, lst_pwd)

        if y == 1:
          p = "R"
          logo.logo_arrow_down()
          lst_pwd = save_pwd(p, lst_pwd)

        if y == -1:
          p = "F"
          logo.logo_arrow_up()
          lst_pwd = save_pwd(p, lst_pwd)

        if -1 < x < 1 and -1 < y < 1:
          p = "N"
          logo.logo_null()
          temp = save_pwd(p, lst_pwd)
          if len(temp) == 2 and temp[1] == False:
            lst_pwd = temp[0]
            not_exit = temp[1]
          else:
            lst_pwd = save_pwd(p, lst_pwd)

    return lst_pwd

##PASSWORD MANAGER - END##
#
##ALPHABET MANAGER - START##
alphabet_list_and_mods =  [
    ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    ["0","1","2","3","4","5","6","7","8","9"],
    ["key",logo.logo_show_msg_edit],
    ["edit",logo.logo_edit_letter_msg],
    ["delete",logo.logo_delete],
    ["back",logo.logo_back]
    ]

alphabet_list = [
    ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    ["0","1","2","3","4","5","6","7","8","9","0"]
    ]


def alphabet_function(msg):
  """
  Allow the user to enter a message with letters and numbers.
  There is four options: show the message, edit a letter, delete a letter and save & back to the 'unlock_menu'.
  
  @pre: 'msg' is a list of strings (which one string is a letter or a number).
  @post: return the list 'msg' modified.
  """
  n = 0
  p = 0
  
  sense.show_letter("A")
  
  while True:
    event = sense.stick.wait_for_event()
    if event.direction == "down" and event.action == "released":
      #Change list 
      n += 1 
          
    if event.direction == "up" and event.action == "pressed": 
      #Change list
      n -= 1
            
    if event.direction =="right" and (event.action == "pressed" or event.action == "held"): 
      #Change list index
      p += 1
          
    if event.direction == "left" and (event.action == "pressed" or event.action == "held"): 
      #Change list index
      p -=1

    n = n % len(alphabet_list_and_mods)
    
    if n < 3: 
      #letters and numbers
      index = p % len(alphabet_list_and_mods[n])
      sense.show_letter(alphabet_list_and_mods[n][index])
      
      if event.action == "pressed" and event.direction == "middle":
        msg.append(alphabet_list_and_mods[n][index])
        
    else:
      #mods
      alphabet_list_and_mods[n][1]()
      if event.action == "pressed" and event.direction == "middle":
          
        if alphabet_list_and_mods[n][0] == "key":
          sense.show_message(msg)
          print("Message seen...")
          
        elif alphabet_list_and_mods[n][0] == "edit":
          edit_code(msg)
          print("Letter edited...")
          
        elif alphabet_list_and_mods[n][0] == "delete":
          del_code(msg)
          print("Letter removed...")
          
        elif alphabet_list_and_mods[n][0] == "back":
          logo.logo_isok()
          return msg
          
     
def edit_code(msg):
    """
    This is a menu to edit (change or delete) a message's letter.
    
    @pre: 'msg' is a list of strings (which one string is a letter or a number).
    @post: return the 'msg' list modified.
    """
    n = 0 
    index = 0
    p = 0
    
    try:
     sense.show_letter(msg[index])
    except:
      sense.show_message("Choose a message before...", scroll_speed = 0.06)
      
    while True:
        
      if not msg:
        break
      else:
        event = sense.stick.wait_for_event()
        if event.direction == "right" and event.action == "pressed":
          if index < len(msg)-1:
            index += 1 
          sense.show_letter(msg[index])
          
        if event.direction == "left" and event.action == "pressed":
          if index > 0:
            index -= 1
          try :
            sense.show_letter(msg[index])
          except :
            break
        
        if event.action =="pressed" and event.direction == "middle":
          modify = modifier_code(msg, index)
          modify
          break
          
          
def modifier_code(msg, index):
  """
  Allow the user to change a letter or number of his own message.
  
  @pre: 'msg' is a list of strings (which one string is a letter or a number).
        'index' is an integer which refers to the index's letter in the user's message.
  @post: return the 'msg' list modified.
  """
  n = 0 
  c = 0
  p = 0
  while 1 > c:
    sense.show_letter(alphabet_list[n][p])
    event = sense.stick.wait_for_event()
    
    if event.action =="pressed" and event.direction == "right":
      if p < len(alphabet_list[n])-1:
        p+= 1 
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "left":
      if p > 0:
        p-= 1 
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "up":
      if n > 0:
        n-=1
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "down":
      if n < len(alphabet_list)-1:
        n+= 1 
      try:
        sense.show_letter(alphabet_list[n][p])
      except:
        p = 0 
        sense.show_letter(alphabet_list[n][p])
        
    if event.action == "pressed" and event.direction == "middle":
      msg[index] = alphabet_list[n][p]
      logo.logo_isok()
      return msg
         
         
def del_code(msg):
  """
  Delete a letter in the user's message.
  
  @pre: 'msg' is a list of strings (which one string is a letter or a number).
  @post: return the 'msg' list modified.
  """
  n = 0 
  c = 0
  p = 0
  
  while 1 > c:
    try:
      sense.show_letter(msg[p])
      
    except:
      try:
        p = 0 
        sense.show_letter(msg[p])
        
      except: 
        sense.show_message("Choose a message before...", scroll_speed = 0.06)
        break
    
    event = sense.stick.wait_for_event()
    
    if event.action =="pressed" and event.direction == "right":
      if p < len(msg)-1:
        p+= 1 
      sense.show_letter(msg[p])
      
    if event.action =="pressed" and event.direction == "left":
      if p > 0:
        p-= 1 
      sense.show_letter(msg[p])
      
    if event.action == "pressed" and event.direction == "middle":
      del msg[p]
      logo.logo_isok()
      return msg

##ALPHABET MANAGER - END##
#
##TIMER FUNCTIONS - START##
def automate_lock():
    """
    Lock automatically the Raspberry Pi.
    
    @pre: 'lock_timer', 'timer_activation' and 'index' need to be defined.
    @post: Interrupt the timer,
           Create a new timer,
           Set 'timer_activation' on True,
           Lock the Raspberry Pi,
           Set 'index' on 0.
    """
    global lock_timer
    global timer_activation
    global index
    lock_timer.cancel() #Reset the timer 'lock_timer'
    lock_timer = Timer(60, automate_lock)
    timer_activation = True
    current_menu[0:1] = [lock_menu]
    current_menu[1:2] = [lock_mode]
    index = 0
    print("Raspberry Pi automatically locked...\n")

lock_timer = Timer(60, automate_lock)

def interrupt_timer():
    """
    Interrupt the locking timer.
    
    @pre: 'lock_timer' and 'timer_activation' need to be defined.
    @post: Interrupt the timer,
           Create a new timer,
           Set 'timer_activation' on True.
    """
    global lock_timer
    global timer_activation
    lock_timer.cancel() #Reset the timer 'timer_cls'
    lock_timer = Timer(60, automate_lock)
    timer_activation = True
    
##TIMER FUNCTIONS - END##
#
##OTHERS FUNCTIONS - START##
def list_to_str (lst):
    """
    Change a list to a string.
    
    @pre: 'lst' is a list.
    @post: return the previous list to a string.
    """
    carac = ""
    for g in range(len(lst)):
        carac += lst[g]
    return carac

def confirme_action():
    """
    Ask the user to confirm the action.
    
    @pre: -
    @post: return a boolean (False if a cross is display and True if a check is display).
    """
    sense.show_message("?", scroll_speed = 0.1)
    selection = True
    index = 0
    logo.logo_delete()
    
    current_logo = [logo.logo_delete, logo.logo_isok_complete]
    while selection:
      events = sense.stick.get_events()
      
      for event in events:
        if event.action != "released":
          if event.direction == "left":
            index -= 1
            
          elif event.direction == "right":
            index += 1
            
          elif event.direction == "middle":
            selection = False
            
          
          if selection:
            current_mode = current_logo[index % 2]
            current_mode()
            
            
      if not selection:
        current_mode = current_logo[index % 2]
        current_mode()
        
    if (index % len(current_logo)) == 1:
      return True
    else:
      return False
        
##OTHERS FUNCTIONS - END##
#
##VARIABLE ESTABLISHMENT 02 - START##
index = 0
unlock_mode = ["show_msg", "change_msg", "delete_msg", "change_password", "delete_password", "lock", "exit"]
lock_mode = ["unlock", "exit"]
current_menu.append(lock_menu)
current_menu.append(lock_mode)

timer_activation = True
##VARIABLE ESTABLISHMENT 02 - END##
#
##PROGRAM START - START##
logo.start_logo_01()
logo.start_logo_02()

while True:
  selection = False
  enter_in_mode = False
  events = sense.stick.get_events()
  
  for event in events:
    if event.action != "released":
      if event.direction == "left":
        index -= 1
        selection = True
        
      elif event.direction == "right":
        index += 1
        selection = True
        
      elif event.direction == "middle":
        enter_in_mode = True
        interrupt_timer()
        
      
      if selection:
        if current_menu[0] == unlock_menu and enter_in_mode == False:
          interrupt_timer()
          
        current_mode = current_menu[1][index % len(current_menu[1])]
        if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
          index = 0
        current_menu[0](current_mode)
        
        
  if not selection:
    if current_menu[0] == unlock_menu and timer_activation == True and enter_in_mode == False:
        lock_timer.start()
        timer_activation = False
    
    current_mode = current_menu[1][index % len(current_menu[1])]
    if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
        index = 0
    current_menu[0](current_mode)

##PROGRAM START - END##
