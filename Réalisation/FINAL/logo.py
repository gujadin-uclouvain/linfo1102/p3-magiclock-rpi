#!/usr/bin/python3
# -*- coding: utf-8 -*

##IMPORT - START##
from sense_hat import SenseHat
import time
##IMPORT - END##
#
##VARIABLE ESTABLISHMENT 01 - START##
sense = SenseHat()
sense.low_light = True
##VARIABLE ESTABLISHMENT 01 - END##
#
##LOGO MANAGER - START##
def start_logo_01():
  """
  Display a red light animation when the Raspberry boots.
  
  @pre: -
  @post: Display a red light animation.
  """
  ##COLOR - START##
  R = (255, 0, 0) #RED
  G = (0, 255, 0) #GREEN
  W = (255, 255, 255) #WHITE
  F = (255, 69, 0) #ORANGE
  O = (0, 0, 0) #BLACK
  ##COLOR - END##
  
  logo_red_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, R, R, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]

  logo_orange_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, F, F, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]

  logo_green_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, G, G, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]
  sense.set_pixels(logo_red_light)
  time.sleep(.4)
  sense.set_pixels(logo_orange_light)
  time.sleep(.5)
  sense.set_pixels(logo_green_light)
  time.sleep(.6)  
  
def start_logo_02():
  """
  Display a start animation when the Raspberry boots.
  
  @pre: -
  @post: Display a start animation.
  """
  ##COLOR - START##
  W = (255, 255, 255) #WHITE
  Y = (255, 255, 0) #YELLOW
  H = (128,128,128) #GREY
  O = (0, 0, 0) #BLACK
  ##COLOR - END##
  
  logo_start_01 = [
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    ]
    
  logo_start_02 = [
    W, W, W, H, H, W, W, W,
    W, W, W, O, O, W, W, W,
    W, W, W, O, O, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    ]
    
  logo_start_03 = [
    W, W, O, H, H, O, W, W,
    W, W, H, O, O, H, W, W,
    W, W, H, O, O, H, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    ]
    
  logo_start_04 = [
    W, O, O, H, H, O, O, W,
    W, O, H, O, O, H, O, W,
    W, O, H, O, O, H, O, W,
    W, O, Y, Y, Y, Y, O, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, O, Y, Y, Y, Y, O, W,
    ]
    
  sense.set_pixels(logo_start_01)
  time.sleep(.15)
  sense.set_pixels(logo_start_02)
  time.sleep(.1)
  sense.set_pixels(logo_start_03)
  time.sleep(.1)
  sense.set_pixels(logo_start_04)
  time.sleep(.1)
  
def end_logo():
  """
  Display an ending animation when the Raspberry shuts off.
  
  @pre: -
  @post: Display an ending animation.
  """
  ##COLOR - START##
  R = (255, 0, 0) #RED
  O = (0, 0, 0) #BLACK
  ##COLOR - END##
  
  logo_end_01 = [
    O, O, O, O, O, O, O, O,
    O, R, O, R, R, O, R, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_02 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_03 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_04 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_05 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_06 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
    
  logo_end_07 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, R, R, R, R, O, O
    ]
    
  sense.set_pixels(logo_end_01)
  time.sleep(.15)
  sense.set_pixels(logo_end_02)
  time.sleep(.1)
  sense.set_pixels(logo_end_03)
  time.sleep(.1)
  sense.set_pixels(logo_end_04)
  time.sleep(.1)
  sense.set_pixels(logo_end_05)
  time.sleep(.1)
  sense.set_pixels(logo_end_06)
  time.sleep(.1)
  sense.set_pixels(logo_end_07)
  time.sleep(.1)
  logo_black()
  time.sleep(.2)
  
def logo_isok():
  """
  Display a green check.
  
  @pre: -
  @post: Display a green check.
  """
  ##COLOR - START##
  G = (0, 255, 0) #GREEN
  O = (0, 0, 0) #BLACK
  ##COLOR - END##
  
  logo_isok_01 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  logo_isok_02 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  logo_isok_03 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, O, O, O, O,
    O, G, G, G, O, O, O, O,
    O, O, G, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]
  logo_isok_03 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, O, O, O, O,
    O, G, G, G, O, O, O, O,
    O, O, G, G, G, O, O, O,
    O, O, O, G, O, O, O, O,
    ]
  logo_isok_04 = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, O, G, G, O, O,
    O, G, G, G, G, G, O, O,
    O, O, G, G, G, O, O, O,
    O, O, O, G, O, O, O, O,
    ]
    
  sense.set_pixels(logo_isok_01)
  time.sleep(.01)
  sense.set_pixels(logo_isok_02)
  time.sleep(.03)
  sense.set_pixels(logo_isok_03)
  time.sleep(.08)
  sense.set_pixels(logo_isok_04)
  time.sleep(.1)
  logo_isok_complete()
  time.sleep(.5)
  
#-------------------------#
def logo_black():
    """
    Display nothing.
  
    @pre: -
    @post: Display nothing.
    """
    ##COLOR - START##
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_black = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_black)

def logo_isok_complete():
    """
    Display one complete check.
  
    @pre: -
    @post: Display one complete check.
    """
    ##COLOR - START##
    G = (0, 255, 0) #GREEN
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_isok_complete = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, G,
        O, O, O, O, O, O, G, G,
        O, G, O, O, O, G, G, G,
        G, G, G, O, G, G, G, O,
        O, G, G, G, G, G, O, O,
        O, O, G, G, G, O, O, O,
        O, O, O, G, O, O, O, O,
        ]
    sense.set_pixels(logo_isok_complete)

def logo_lock():
    """
    Display a closed padlock.
  
    @pre: -
    @post: Display a closed padlock.
    """
    ##COLOR - START##
    Y = (255, 255, 0) #YELLOW
    H = (128,128,128) #GREY
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_lock = [
        O, O, O, H, H, O, O, O,
        O, O, H, O, O, H, O, O,
        O, O, H, O, O, H, O, O,
        O, O, Y, Y, Y, Y, O, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, O, Y, Y, Y, Y, O, O,
        ]
    sense.set_pixels(logo_lock)

def logo_unlock_lock():
    """
    Display a half-closed padlock.
  
    @pre: -
    @post: Display a half-closed padlock.
    """
    ##COLOR - START##
    Y = (255, 255, 0) #YELLOW
    H = (128,128,128) #GREY
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_unlock_lock = [
        O, O, O, H, H, O, O, O,
        O, O, H, O, O, H, O, O,
        O, O, O, O, O, H, O, O,
        O, O, Y, Y, Y, Y, O, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, O, Y, Y, Y, Y, O, O,
        ]
    sense.set_pixels(logo_unlock_lock)

def logo_unlock():
    """
    Display an opened padlock.
  
    @pre: -
    @post: Display an opened padlock.
    """
    ##COLOR - START##
    Y = (255, 255, 0) #YELLOW
    H = (128,128,128) #GREY
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_unlock = [
        O, O, O, H, H, O, O, O,
        O, O, O, O, O, H, O, O,
        O, O, O, O, O, H, O, O,
        O, O, Y, Y, Y, Y, O, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, Y, Y, Y, Y, Y, Y, O,
        O, O, Y, Y, Y, Y, O, O,
        ]
    sense.set_pixels(logo_unlock)

def logo_show_msg():
    """
    Display a white letter.
  
    @pre: -
    @post: Display a white letter.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_show_msg = [
        W, W, W, W, W, W, W, W,
        W, O, O, O, O, O, O, W,
        W, W, O, O, O, O, W, W,
        W, O, W, O, O, W, O, W,
        W, O, O, W, W, O, O, W,
        W, O, O, O, O, O, O, W,
        W, O, O, O, O, O, O, W,
        W, W, W, W, W, W, W, W
        ]
    sense.set_pixels(logo_show_msg)

def logo_show_msg_edit():
    """
    Display a white and green letter.
  
    @pre: -
    @post: Display a white and green letter.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    G = (0, 255, 0) #GREEN
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_show_msg_edit = [
        W, W, W, W, W, W, W, W,
        W, O, O, O, O, O, O, W,
        W, G, O, O, O, O, G, W,
        W, O, G, O, O, G, O, W,
        W, O, O, G, G, O, O, W,
        W, O, O, O, O, O, O, W,
        W, O, O, O, O, O, O, W,
        W, W, W, W, W, W, W, W
        ]
    sense.set_pixels(logo_show_msg_edit)

def logo_change_msg():
    """
    Display a green pencil.
  
    @pre: -
    @post: Display a green pencil.
    """
    ##COLOR - START##
    G = (0, 255, 0) #GREEN
    P = (255,105, 180) #PINK
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_change_msg = [
        O, O, O, O, O, O, O, O,
        O, O, G, O, O, O, O, O,
        O, G, G, G, O, O, O, O,
        O, O, G, G, G, O, O, O,
        O, O, O, G, G, G, O, O,
        O, O, O, O, G, G, G, O,
        O, O, O, O, O, G, P, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_change_msg)

def logo_delete_msg():
    """
    Display a white letter with a red cross above.
  
    @pre: -
    @post: Display .
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_delete_msg = [
        R, W, W, W, W, W, W, R,
        W, R, O, O, O, O, R, W,
        W, W, R, O, O, R, W, W,
        W, O, W, R, R, W, O, W,
        W, O, O, R, R, O, O, W,
        W, O, R, O, O, R, O, W,
        W, R, O, O, O, O, R, W,
        R, W, W, W, W, W, W, R
        ]
    sense.set_pixels(logo_delete_msg)

def logo_sword():
    """
    Display a sword.
  
    @pre: -
    @post: Display a sword.
    """
    ##COLOR - START##
    H = (128,128,128) #GREY
    N = (128, 0, 0) #BROWN
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_sword = [
        O, O, O, O, O, O, H, H,
        O, O, O, O, O, H, H, H,
        O, O, O, O, H, H, H, O,
        N, N, O, H, H, H, O, O,
        O, N, H, H, H, O, O, O,
        O, O, N, H, O, O, O, O,
        O, N, O, N, N, O, O, O,
        N, O, O, O, N, O, O, O,
        ]
    sense.set_pixels(logo_sword)

def logo_key():
    """
    Display a grey key.
  
    @pre: -
    @post: Display a grey key.
    """
    ##COLOR - START##
    H = (128,128,128) #GREY
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_key = [
        O, O, O, H, H, O, O, O,
        O, O, H, O, O, H, O, O,
        O, O, O, H, H, O, O, O,
        O, O, O, H, O, O, O, O,
        O, O, O, H, O, O, O, O,
        O, O, O, H, H, H, O, O,
        O, O, O, H, O, O, O, O,
        O, O, O, H, H, H, O, O,
        ]
    sense.set_pixels(logo_key)

def logo_delete_key():
    """
    Display a grey key with a red cross above.
  
    @pre: -
    @post: Display a grey key.
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    H = (128,128,128) #GREY
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_delete_key = [
        R, O, O, H, H, O, O, R,
        O, R, H, O, O, H, R, O,
        O, O, R, H, H, R, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, R, H, H, R, O, O,
        O, R, O, H, O, O, R, O,
        R, O, O, H, H, H, O, R,
        ]
    sense.set_pixels(logo_delete_key)

def logo_edit_letter_msg():
    """
    Display a red pencil.
  
    @pre: -
    @post: Display a red pencil.
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    P = (255,105, 180) #PINK
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_edit_letter_msg = [
        O, O, O, O, O, O, O, O,
        O, O, R, O, O, O, O, O,
        O, R, R, R, O, O, O, O,
        O, O, R, R, R, O, O, O,
        O, O, O, R, R, R, O, O,
        O, O, O, O, R, R, R, O,
        O, O, O, O, O, R, P, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_edit_letter_msg)

def logo_delete():
    """
    Display a red cross.
  
    @pre: -
    @post: Display a red cross.
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_delete = [
        R, R, O, O, O, O, R, R,
        R, R, R, O, O, R, R, R,
        O, R, R, R, R, R, R, O,
        O, O, R, R, R, R, O, O,
        O, O, R, R, R, R, O, O,
        O, R, R, R, R, R, R, O,
        R, R, R, O, O, R, R, R,
        R, R, O, O, O, O, R, R,
        ]
    sense.set_pixels(logo_delete)

def logo_back():
    """
    Display a red and white back arrow.
  
    @pre: -
    @post: Display a red and white back arrow.
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_back = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, R, O, O, W, O,
        O, O, R, O, O, W, W, O,
        O, R, W, W, W, W, O, O,
        O, O, R, O, O, O, O, O,
        O, O, O, R, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_back)

def logo_exit():
    """
    Display the shutdown symbol.
  
    @pre: -
    @post: Display the shutdown symbol.
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_exit = [
        O, O, O, R, R, O, O, O,
        O, R, O, R, R, O, R, O,
        R, O, O, R, R, O, O, R,
        R, O, O, R, R, O, O, R,
        R, O, O, O, O, O, O, R,
        R, O, O, O, O, O, O, R,
        O, R, O, O, O, O, R, O,
        O, O, R, R, R, R, O, O
        ]
    sense.set_pixels(logo_exit)
##LOGO MANAGER - END##
#
##LOGO POSITION MANAGER - START##
def logo_null():
    """
    Display a white point.
  
    @pre: -
    @post: Display a white point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, W, W, O, O, O,
        O, O, O, W, W, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)

def logo_null01():
    """
    Display a part of a white point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, R, W, O, O, O,
        O, O, O, W, W, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)
    
def logo_null02():
    """
    Display a part of a white point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, W, W, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)
    
def logo_null03():
    """
    Display a part of a white point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, W, R, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)

def logo_null_reboot():
    """
    Display a red point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)
    
def logo_null_reboot01():
    """
    Display a part of a red point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, W, R, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)
    
def logo_null_reboot02():
    """
    Display a part of a red point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, W, W, O, O, O,
        O, O, O, R, R, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)
    
def logo_null_reboot03():
    """
    Display a part of a red point.
  
    @pre: -
    @post: Display a red point.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    R = (255, 0, 0) #RED
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_null = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, W, W, O, O, O,
        O, O, O, R, W, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_null)

def logo_arrow_right():
    """
    Display an arrow turned to the right.
  
    @pre: -
    @post: Display an arrow turned to the right.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, W, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)

def logo_arrow_right01():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)



def logo_arrow_right02():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)

def logo_arrow_right03():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)
    
def logo_arrow_right04():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)

def logo_arrow_right05():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, W, O,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)

def logo_arrow_right06():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, W, O, O,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)

def logo_arrow_right07():
    """
        Display half of a red arrow turned to the right.
        
        @pre: -
        @post: Display half of a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, W, O, O, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)


def logo_arrow_right08():
    """
        Display a red arrow turned to the right.
        
        @pre: -
        @post: Display a red arrow turned to the right.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_right = [
        O, O, O, O, R, O, O, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, O, R,
        O, O, O, O, O, O, R, O,
        O, O, O, O, O, R, O, O,
        O, O, O, O, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_right)


def logo_arrow_left():
    """
        Display an arrow turned to the left.
        
        @pre: -
        @post: Display an arrow turned to the left.
        """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, O, O, W, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)


def logo_arrow_left01():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)


def logo_arrow_left02():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)

def logo_arrow_left03():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)



def logo_arrow_left04():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       W, O, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)

def logo_arrow_left05():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, W, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)

def logo_arrow_left06():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, W, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)

def logo_arrow_left07():
    """
        Display half of a red arrow turned to the left.
        
        @pre: -
        @post: Display half of a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, W, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)


def logo_arrow_left08():
    """
        Display a red arrow turned to the left.
        
        @pre: -
        @post: Display a red arrow turned to the left.
        """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_left = [
       O, O, O, R, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       R, O, O, O, O, O, O, O,
       O, R, O, O, O, O, O, O,
       O, O, R, O, O, O, O, O,
       O, O, O, R, O, O, O, O,
       ]
    sense.set_pixels(logo_arrow_left)

def logo_arrow_up():
    """
    Display an up arrow.
  
    @pre: -
    @post: Display an up arrow.
    """
    ##COLOR - START##
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, W, W, O, O, O,
        O, O, W, O, O, W, O, O,
        O, W, O, O, O, O, W, O,
        W, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up01():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, W, W, O, O, O,
        O, O, W, O, O, W, O, O,
        O, W, O, O, O, O, W, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up02():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, W, W, O, O, O,
        O, O, W, O, O, W, O, O,
        O, R, O, O, O, O, W, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up03():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, W, W, O, O, O,
        O, O, R, O, O, W, O, O,
        O, R, O, O, O, O, W, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up04():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, R, W, O, O, O,
        O, O, R, O, O, W, O, O,
        O, R, O, O, O, O, W, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up05():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, R, R, O, O, O,
        O, O, R, O, O, W, O, O,
        O, R, O, O, O, O, W, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up06():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, R, R, O, O, O,
        O, O, R, O, O, R, O, O,
        O, R, O, O, O, O, W, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up07():
    """
    Display half of a red up arrow.
  
    @pre: -
    @post: Display half of a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, R, R, O, O, O,
        O, O, R, O, O, R, O, O,
        O, R, O, O, O, O, R, O,
        R, O, O, O, O, O, O, W,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)
    
def logo_arrow_up08():
    """
    Display a red up arrow.
  
    @pre: -
    @post: Display a red up arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_up = [
        O, O, O, R, R, O, O, O,
        O, O, R, O, O, R, O, O,
        O, R, O, O, O, O, R, O,
        R, O, O, O, O, O, O, R,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        ]
    sense.set_pixels(logo_arrow_up)

def logo_arrow_down():
    """
    Display a down arrow.
  
    @pre: -
    @post: Display a down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        W, O, O, O, O, O, O, W,
        O, W, O, O, O, O, W, O,
        O, O, W, O, O, W, O, O,
        O, O, O, W, W, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down01():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display half of a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, W, O, O, O, O, W, O,
        O, O, W, O, O, W, O, O,
        O, O, O, W, W, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down02():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display half of a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, R, O, O, O, O, W, O,
        O, O, W, O, O, W, O, O,
        O, O, O, W, W, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down03():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display half of a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, R, O, O, O, O, W, O,
        O, O, R, O, O, W, O, O,
        O, O, O, W, W, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down04():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display half of a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, R, O, O, O, O, W, O,
        O, O, R, O, O, W, O, O,
        O, O, O, R, W, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down05():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display ahalf red  down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, R, O, O, O, O, W, O,
        O, O, R, O, O, W, O, O,
        O, O, O, R, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down06():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display half of a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, R, O, O, O, O, W, O,
        O, O, R, O, O, R, O, O,
        O, O, O, R, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down07():
    """
    Display half of a red down arrow.
  
    @pre: -
    @post: Display half of a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, W,
        O, R, O, O, O, O, R, O,
        O, O, R, O, O, R, O, O,
        O, O, O, R, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
    
def logo_arrow_down08():
    """
    Display a red down arrow.
  
    @pre: -
    @post: Display a red down arrow.
    """
    ##COLOR - START##
    R = (225, 0, 0) #RED
    W = (255, 255, 255) #WHITE
    O = (0, 0, 0) #BLACK
    ##COLOR - END##
    
    logo_arrow_down = [
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        O, O, O, O, O, O, O, O,
        R, O, O, O, O, O, O, R,
        O, R, O, O, O, O, R, O,
        O, O, R, O, O, R, O, O,
        O, O, O, R, R, O, O, O,
        ]
    sense.set_pixels(logo_arrow_down)
##LOGO POSITION MANAGER - END##
