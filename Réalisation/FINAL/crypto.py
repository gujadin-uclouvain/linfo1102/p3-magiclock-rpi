#!/usr/bin/python3
# -*- coding: utf-8 -*

"""
Librairie de cryptographie rendue disponible pour le P3 du cours de LINFO1102.

Attention:  Cette librairie a été réalisée à des fins purement didactiques et ne peut en aucun cas être considérée comme
            une solution cryptographique viable pour un programme en dehors du contexte du cours LINFO1102.
"""

def encode(pwd, plain_text):
    """
    Crypte un texte en utilisant une clé de chiffrement, tous les deux fournis sous la forme d'une chaine de caractères.
    L'algorithme utilisé est le chiffrement de Vigenère.
    Attention : cette méthode est "craquée" depuis longtemps, mais elle illustre le fonctionnement d'un algorithme de chiffrement.

    :param (str) pwd: la clé de chiffrement
    :param (str) plain_text: le texte à chiffrer
    :return (str): le texte chiffré
    """
    key = pwd
    enc = []
    for i, e in enumerate(plain_text):
        key_c = key[i % len(key)]
        enc_c = chr((ord(e) + ord(key_c)) % 256)
        enc.append(enc_c)
    return ("".join(enc).encode()).decode()

def decode(pwd, cipher_text):
    """
    Déchiffre le texte en utilisant la clé de déchiffrement, tous les deux fournis sous la forme d'une chaine de caractères.
    L'algorithme utilisé est le (dé)chiffrement de Vigenère.
    Attention : cette méthode est "craquée" depuis longtemps, mais elle illustre le fonctionnement d'un algorithme de (dé-)chiffrement.

    :param (str) pwd: le mot de passe rentré par l'utilisateur
    :param (str) cipher_text: le texte crypté
    :return (str): le texte décrypté
    """
    key = pwd
    dec = []
    for i, e in enumerate(cipher_text):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(e) - ord(key_c)) % 256)
        dec.append(dec_c)
    return str("".join(dec))

def hashing(pwd):
    """
    Hachage d'un mot de passe fourni en entrée.
    Le résultat est une chaîne de caractères.
    Attention : cette technique de hachage n'est pas suffisante (hachage dit cryptographique) pour une utilisation en dehors du cours.

    :param (str) pwd: le mot de passe sous forme de chaîne de caractères
    :return (str): le résultat du hachage
    """
    def to_32(value):
        """
        Fonction interne utilisée par hashing.
        Convertit une valeur en un entier signé de 32 bits.
        Si value est un entier trop long de base, on en prend qu'une version diminuée.

        :param (int) value: valeur du caractère transformé par la valeur de hachage de cette itération
        :return (int): entier signé de 32 bits représentant value
        """
        value = value % (2 ** 32)
        if value >= 2**31:
            value = value - 2 ** 32
        value = int(value)
        return value

    if pwd:
        x = ord(pwd[0]) << 7
        m = 1000003
        for c in pwd:
            x = to_32((x*m) ^ ord(c))
        x ^= len(pwd)
        if x == -1:
            x = -2
        return str(x)
    return