##IMPORT - START##
from sense_hat import SenseHat
from os import system
import time
##IMPORT - END##
#
##VARIABLE ESTABLISHMENT 01 - START##
sense = SenseHat()
sense.low_light = True
current_menu = []
##VARIABLE ESTABLISHMENT 01 - END##
#
##COLOR - START##
R = (255, 0, 0) #RED
G = (0, 255, 0) #GREEN
B = (0, 0, 255) #BLUE
W = (255, 255, 255) #WHITE
Y = (255, 255, 0) #YELLOW
H = (128,128,128) #GREY
N = (128, 0, 0) #BROWN
F = (255, 69, 0) #ORANGE
P = (255,105, 180) #PINK
O = (0, 0, 0) #BLACK
##COLOR - END##
#
##LOGO - START##
def start_logo_01():
  logo_red_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, R, R, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]

  logo_orange_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, F, F, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]

  logo_green_light = [
    O, O, W, W, W, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, O, O, W, O, O,
    O, O, W, G, G, W, O, O,
    O, O, W, W, W, W, O, O,
    O, O, O, W, W, O, O, O
    ]
  sense.set_pixels(logo_red_light)
  time.sleep(.4)
  sense.set_pixels(logo_orange_light)
  time.sleep(.5)
  sense.set_pixels(logo_green_light)
  time.sleep(.6)  
  
def start_logo_02():
  logo_start_01 = [
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    W, W, W, W, W, W, W, W,
    ]
    
  logo_start_02 = [
    W, W, W, H, H, W, W, W,
    W, W, W, O, O, W, W, W,
    W, W, W, O, O, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    W, W, W, Y, Y, W, W, W,
    ]
    
  logo_start_03 = [
    W, W, O, H, H, O, W, W,
    W, W, H, O, O, H, W, W,
    W, W, H, O, O, H, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    W, W, Y, Y, Y, Y, W, W,
    ]
    
  logo_start_04 = [
    W, O, O, H, H, O, O, W,
    W, O, H, O, O, H, O, W,
    W, O, H, O, O, H, O, W,
    W, O, Y, Y, Y, Y, O, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, Y, Y, Y, Y, Y, Y, W,
    W, O, Y, Y, Y, Y, O, W,
    ]
    
  sense.set_pixels(logo_start_01)
  time.sleep(.15)
  sense.set_pixels(logo_start_02)
  time.sleep(.1)
  sense.set_pixels(logo_start_03)
  time.sleep(.1)
  sense.set_pixels(logo_start_04)
  time.sleep(.1)
#-------------------------#
logo_black = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, O, O,
    ]

logo_lock = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, H, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_unlock_lock = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, O, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_unlock = [
    O, O, O, H, H, O, O, O,
    O, O, O, O, O, H, O, O,
    O, O, O, O, O, H, O, O,
    O, O, Y, Y, Y, Y, O, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, Y, Y, Y, Y, Y, Y, O,
    O, O, Y, Y, Y, Y, O, O,
    ]

logo_show_msg = [
    W, W, W, W, W, W, W, W,
    W, O, O, O, O, O, O, W,
    W, W, O, O, O, O, W, W,
    W, O, W, O, O, W, O, W,
    W, O, O, W, W, O, O, W,
    W, O, O, O, O, O, O, W,
    W, O, O, O, O, O, O, W,
    W, W, W, W, W, W, W, W
    ]

logo_change_msg = [
    R, W, W, W, W, W, W, R,
    W, R, O, O, O, O, R, W,
    W, W, R, O, O, R, W, W,
    W, O, W, R, R, W, O, W,
    W, O, O, R, R, O, O, W,
    W, O, R, O, O, R, O, W,
    W, R, O, O, O, O, R, W,
    R, W, W, W, W, W, W, R
    ]

logo_change_password = [
    O, O, O, O, O, O, H, H,
    O, O, O, O, O, H, H, H,
    O, O, O, O, H, H, H, O,
    N, N, O, H, H, H, O, O,
    O, N, H, H, H, O, O, O,
    O, O, N, H, O, O, O, O,
    O, N, O, N, N, O, O, O,
    N, O, O, O, N, O, O, O,
    ]

logo_key = [
    O, O, O, H, H, O, O, O,
    O, O, H, O, O, H, O, O,
    O, O, O, H, H, O, O, O,
    O, O, O, H, O, O, O, O,
    O, O, O, H, O, O, O, O,
    O, O, O, H, H, H, O, O,
    O, O, O, H, O, O, O, O,
    O, O, O, H, H, H, O, O,
    ]
    
logo_back = [
    O, O, O, O, O, O, O, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, W, O,
    O, O, O, O, O, O, W, O,
    O, O, R, O, O, W, W, O,
    O, R, W, W, W, W, O, O,
    O, O, R, O, O, O, O, O,
    ]
    
logo_delete_letter_msg = [
    O, O, O, O, O, O, O, O,
    O, O, R, O, O, O, O, O,
    O, R, R, R, O, O, O, O,
    O, O, R, R, R, O, O, O,
    O, O, O, R, R, R, O, O,
    O, O, O, O, R, R, R, O,
    O, O, O, O, O, R, P, O,
    O, O, O, O, O, O, O, O,
    ]

logo_exit = [
    O, O, O, R, R, O, O, O,
    O, R, O, R, R, O, R, O,
    R, O, O, R, R, O, O, R,
    R, O, O, R, R, O, O, R,
    R, O, O, O, O, O, O, R,
    R, O, O, O, O, O, O, R,
    O, R, O, O, O, O, R, O,
    O, O, R, R, R, R, O, O
    ]
##LOGO - END##
#
##[MAIN MENU] - LOCK & UNLOCK MENU -- START##
def unlock_menu(unlock_mode):
  if unlock_mode == "show_msg":
    sense.set_pixels(logo_show_msg)
    if enter_in_mode:
      alphabet_function()
    
  elif unlock_mode == "change_msg":
    sense.set_pixels(logo_change_msg)
    if enter_in_mode:
      alphabet_function()
    
  elif unlock_mode == "change_password":
    sense.set_pixels(logo_change_password)
    
  elif unlock_mode == "lock":
    sense.set_pixels(logo_unlock)
    if enter_in_mode:
      current_menu[0:1] = [lock_menu]
      current_menu[1:2] = [lock_mode]
      sense.set_pixels(logo_unlock_lock)
      time.sleep(.2)
      sense.set_pixels(logo_lock)
      time.sleep(.2)
    
  elif unlock_mode == "exit":
    sense.set_pixels(logo_exit)
    if enter_in_mode:
      sense.set_pixels(logo_black)
      system("sudo shutdown now")
   
#-----------------------------------------#
def lock_menu(lock_mode):
  if lock_mode == "unlock":
    sense.set_pixels(logo_lock)
    if enter_in_mode:
      current_menu[0:1] = [unlock_menu]
      current_menu[1:2] = [unlock_mode]
      sense.set_pixels(logo_unlock_lock)
      time.sleep(.2)
      sense.set_pixels(logo_unlock)
      time.sleep(.2)
    
  elif lock_mode == "exit":
    sense.set_pixels(logo_exit)
    if enter_in_mode:
      sense.set_pixels(logo_black)
      system("sudo shutdown now")
##[MAIN MENU] - LOCK & UNLOCK MENU -- END##
#
##ALPHABET - START##
alphabet_list_and_mods =  [
    ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    ["0","1","2","3","4","5","6","7","8","9","0"],
    [logo_key],
    [logo_delete_letter_msg],
    [logo_back]
    ]

alphabet_list = [
    ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"],
    ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
    ["0","1","2","3","4","5","6","7","8","9","0"]
    ]

def alphabet_function():
  n = 0
  p = 0
  code = []
  sense.show_letter("A")
  
  while True:
    event = sense.stick.wait_for_event()
    
    if event.direction == "down" and event.action == "released":#Change list + bug correction
      if n < len(alphabet_list_and_mods)-1:
        n += 1 
      try :
        sense.show_letter(alphabet_list_and_mods[n][p])
        
      except : 
        try :
          p=0
          sense.show_letter(alphabet_list_and_mods[n][p])
          
        except :
            p = 0
            sense.set_pixels(alphabet_list_and_mods[n][p])
          
          
    if event.direction == "up" and event.action == "pressed": #Change list
      if n > 0:
        n -= 1
      try :
        sense.show_letter(alphabet_list_and_mods[n][p])
        
      except : 
        try :
          p=0
          sense.show_letter(alphabet_list_and_mods[n][p])
          
        except :
            p = 0
            sense.set_pixels(alphabet_list_and_mods[n][p])
            
    if event.direction =="right" and (event.action == "pressed" or event.action == "held"): #Change list
      if p < len(alphabet_list_and_mods[n])-1 :
        p += 1
      try :
        sense.show_letter(alphabet_list_and_mods[n][p])
      except:
        try:
          sense.set_pixels(alphabet_list_and_mods[n][p])
        except:
          p = 0
          sense.show_message(alphabet_list_and_mods[n][p])
          
    if event.direction == "left" and (event.action == "pressed" or event.action == "held"): #Change list index
      if p > 0:
        p-=1
      try :
        sense.show_letter(alphabet_list_and_mods[n][p])
      except:
        try:
          sense.set_pixels(alphabet_list_and_mods[n][p])
        except:
          p = 0
          sense.show_message(alphabet_list_and_mods[n][p])
          
    if event.action == "pressed" and event.direction == "middle" and alphabet_list_and_mods[n][p] == str(alphabet_list_and_mods[n][p]):
      code.append(alphabet_list_and_mods[n][p])
      
    if alphabet_list_and_mods[n][p] == logo_key and event.action == "pressed" and event.direction == "middle":
      sense.show_message(code)
      print(code)
      p = 0
      n = 0
      sense.show_letter(alphabet_list_and_mods[n][p])
      
    if alphabet_list_and_mods[n][p] == logo_delete_letter_msg and event.action == "pressed" and event.direction == "middle":
      edit = supr_code(code)
      edit
    
    if alphabet_list_and_mods[n][p] == logo_back and event.action == "pressed" and event.direction == "middle":
      break
    
def supr_code(code):
    n = 0 
    c = 0
    p = 0
    
    try:
     sense.show_letter(code[c])
    except:
      sense.show_message("Edit a code")
      
    while True:
      event = sense.stick.wait_for_event()
      
      if event.direction == "right" and event.action == "pressed":
        if c < len(code)-1:
          c += 1 
        sense.show_letter(code[c])
        
      if event.direction == "left" and event.action == "pressed":
        if c > 0:
          c -= 1 
        try :
          sense.show_letter(code[c])
        except :
          break
        
      if event.action =="pressed" and event.direction == "down":
        del code[c]
        
      if event.action =="pressed" and event.direction == "up":
        modify = modifier_code(code)
        modify
        
def modifier_code(code):
  n = 0 
  c = 0
  p = 0
  
  while 1 > c:
    sense.show_letter(alphabet_list[n][p])
    event = sense.stick.wait_for_event()
    
    if event.action =="pressed" and event.direction == "right":
      if p < len(alphabet_list[n])-1:
        p+= 1 
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "left":
      if p > 0:
        p-= 1 
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "up":
      if n > 0:
        n-=1
      sense.show_letter(alphabet_list[n][p])
      
    if event.action =="pressed" and event.direction == "down":
      if n < len(alphabet_list)-1:
        n+= 1 
      try:
        sense.show_letter(alphabet_list[n][p])
      except:
        p = 0 
        sense.show_letter(alphabet_list[n][p])
        
    if event.action == "pressed" and event.direction == "middle":
      code[n] = alphabet_list[n][p]
      break
        
##ALPHABET - END##
#
##VARIABLE ESTABLISHMENT 02 - START##
index = 0
unlock_mode = ["show_msg", "change_msg", "change_password", "lock", "exit"]
lock_mode = ["unlock", "exit"]
current_menu.append(lock_menu)
current_menu.append(lock_mode)
##VARIABLE ESTABLISHMENT 02 - END##
#
##PROGRAM START - START##
start_logo_01()
start_logo_02()
while True:
  selection = False
  enter_in_mode = False
  events = sense.stick.get_events()
  
  for event in events:
    if event.action != "released":
      if event.direction == "left":
        index -= 1
        selection = True
        
      elif event.direction == "right":
        index += 1
        selection = True
        
      elif event.direction == "middle":
        enter_in_mode = True
        
      
      if selection:
        current_mode = current_menu[1][index % len(current_menu[1])]
        if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
          index = 0
        current_menu[0](current_mode)
        
        
  if not selection:
    current_mode = current_menu[1][index % len(current_menu[1])]
    if (current_mode == "lock" or current_mode == "unlock") and enter_in_mode == True:
        index = 0
    current_menu[0](current_mode)
##PROGRAM START - END##
