from sense_hat import SenseHat
import time

sense = SenseHat()

##COLOR - START##
G = (0, 255, 0) #GREEN
O = (0, 0, 0) #BLACK
##COLOR - END##

fleche_avant = [
    O, O, O, G, O, O, O, O,
    O, O, G, O, O, O, O, O,
    O, G, O, O, O, O, O, O,
    G, G, G, G, G, G, G, G,
    G, G, G, G, G, G, G, G,
    O, G, O, O, O, O, O, O,
    O, O, G, O, O, O, O, O,
    O, O, O, G, O, O, O, O
]

fleche_arriere = [
    O, O, O, O, G, O, O, O,
    O, O, O, O, O, G, O, O,
    O, O, O, O, O, O, G, O,
    G, G, G, G, G, G, G, G,
    G, G, G, G, G, G, G, G,
    O, O, O, O, O, O, G, O,
    O, O, O, O, O, G, O, O,
    O, O, O, O, G, O, O, O
]

fleche_gauche = [
    O, O, O, G, G, O, O, O,
    O, O, G, G, G, G, O, O,
    O, G, O, G, G, O, G, O,
    G, O, O, G, G, O, O, G,
    O, O, O, G, G, O, O, O,
    O, O, O, G, G, O, O, O,
    O, O, O, G, G, O, O, O,
    O, O, O, G, G, O, O, O
]

fleche_droite = [
    O, O, O, G, G, O, O, O,
    O, O, O, G, G, O, O, O,
    O, O, O, G, G, O, O, O,
    O, O, O, G, G, O, O, O,
    G, O, O, G, G, O, O, G,
    O, G, O, G, G, O, G, O,
    O, O, G, G, G, G, O, O,
    O, O, O, G, G, O, O, O
]

while True:
    x, y, z = sense.get_accelerometer_raw().values()
    
    if x > 0.75:
        sense.set_pixels(fleche_droite)
    elif x < -0.75:
        sense.set_pixels(fleche_gauche)
    elif y > 0.75:
        sense.set_pixels(fleche_arriere)
    elif y < -0.75:
        sense.set_pixels(fleche_avant)
    elif z > 2:
      sense.show_letter("B")
    elif z < - 0.5:
        sense.show_letter("H")

    elif -0.5 < z < 2 and -0.75 < y < 0.75 and -0.75 < x < 0.75:
        sense.show_letter("N")

    