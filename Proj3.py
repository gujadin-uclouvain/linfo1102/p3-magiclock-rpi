
from sense_hat import SenseHat, ACTION_PRESSED
import time
from signal import pause

s = SenseHat()

  
Alphabet = (("A","a") , ("B","b") , ("C","c") , ("D","d"), ("E","e"), ("F","f"),("G","g"),("H","h"),("I","i"),("J","j"),("K","k"),("L","l"),("M","m"),("N","n"),("O","o"),("P","p"),("Q","q"),("R","r"),("S","s"),("T","t"),("U","u"),("V","v"),("W","w"),("X","x"),("Y","y"),("Z","z"))
def Alphab():
  n = 0
  p = 0
  while True:
    event = s.stick.wait_for_event()
    if event.direction == "right":
      if n < len(Alphabet)-1:
        n += 1 
      s.show_letter(Alphabet[n][p])
    if event.direction == "left":
      if n > -len(Alphabet):
        n -= 1
      s.show_letter(Alphabet[n][p])
    if event.direction =="down":
      if p != 1:
        p += 1
    if event.direction == "up":
      if p!= 0:
        p-=1
      
Alphab()